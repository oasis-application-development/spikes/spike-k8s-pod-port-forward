### Spike on Running Pod in 1 cluster that port-forwards to a service in another cluster

Given 2 separate kubernetes clusters, [a] and [b], is it possible to run
- a service + pod in [b]
- a pod in [a]

such that the [a] pod has 2 containers:
- (C) starts a kubectl port-forward to the service in [b]
- (W) runs a process that accesses the port-forwarded service


### Steps

1. login to cluster b

2. create b-service
```
kubectl create -f b-service.yml
```

3. login to cluster a

4. create a-pod-env.nocommit.yml using a-pod-env.example.yml if it does not already exist.
This will contain the url to the kubernetes api for cluster b, a namespace in b, and a token
with at least the ability to port-forward to services in cluster b.

5. create the env and pod
```
k create -f a-pod-env.nocommit.env
k create -f a-pod.env
k attach a-pod --container a-pod-process
$ curl localhost:8080
```
